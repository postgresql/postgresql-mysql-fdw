CREATE DATABASE mysql_fdw_regress;
CREATE DATABASE mysql_fdw_regress1;
CREATE USER 'edb'@'localhost' IDENTIFIED BY 'edb';
GRANT ALL PRIVILEGES ON mysql_fdw_regress.* TO 'edb'@'localhost';
GRANT ALL PRIVILEGES ON mysql_fdw_regress1.* TO 'edb'@'localhost';
