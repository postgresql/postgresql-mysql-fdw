postgresql-mysql-fdw (2.9.2-2) unstable; urgency=medium

  * Upload for PostgreSQL 17.
  * Restrict to 64-bit architectures.

 -- Christoph Berg <myon@debian.org>  Sat, 14 Sep 2024 21:28:54 +0000

postgresql-mysql-fdw (2.9.2-1) unstable; urgency=medium

  * New upstream version 2.9.2.
  * Bump debian/pgversions to 12+.

 -- Christoph Berg <myon@debian.org>  Fri, 19 Jul 2024 19:18:07 +0200

postgresql-mysql-fdw (2.9.1-2) unstable; urgency=medium

  * Upload for PostgreSQL 16.
  * Use ${postgresql:Depends}.

 -- Christoph Berg <myon@debian.org>  Mon, 18 Sep 2023 21:27:32 +0200

postgresql-mysql-fdw (2.9.1-1) unstable; urgency=medium

  * New upstream version 2.9.1.
  * Skip tests when mysql schema can't be loaded. (It's incompatible with
    mariadb now.)
  * Adjust tests for changed mysql error messages. (Closes: #1033836)
  * Remove pushdown test with varying output on PG11.
  * Give up on playing whack-a-mole with upstream regression tests and mark
    them as flaky; add a simple superficial "create extension" test.

 -- Christoph Berg <myon@debian.org>  Tue, 18 Jul 2023 12:59:44 +0200

postgresql-mysql-fdw (2.8.0-3) unstable; urgency=medium

  * Upload for PostgreSQL 15.

 -- Christoph Berg <myon@debian.org>  Mon, 24 Oct 2022 16:01:20 +0200

postgresql-mysql-fdw (2.8.0-2) unstable; urgency=medium

  * Test-depend on locales-all instead of pgcommon.sh.

 -- Christoph Berg <myon@debian.org>  Fri, 19 Aug 2022 15:40:10 +0200

postgresql-mysql-fdw (2.8.0-1) unstable; urgency=medium

  * New upstream version 2.8.0.
  * Fix unstable ordering in sql/select.sql.
  * Disable fetch_size error tests so we don't have to maintain a separate
    expected output file.

 -- Christoph Berg <myon@debian.org>  Fri, 20 May 2022 14:56:44 +0200

postgresql-mysql-fdw (2.7.0-3) unstable; urgency=medium

  * debian/tests: Skip tests if mariadb/mysql doesn't start.

 -- Christoph Berg <myon@debian.org>  Mon, 28 Feb 2022 11:37:12 +0100

postgresql-mysql-fdw (2.7.0-2) unstable; urgency=medium

  * Disable jit for tests; works around "failed to resolve name __mulodi4".
    Cf. https://github.com/EnterpriseDB/mysql_fdw/pull/227.
    (Closes: 1001811)

 -- Christoph Berg <myon@debian.org>  Wed, 12 Jan 2022 17:28:21 +0100

postgresql-mysql-fdw (2.7.0-1) unstable; urgency=medium

  * New upstream version 2.7.0.

 -- Christoph Berg <myon@debian.org>  Mon, 06 Dec 2021 12:58:34 +0100

postgresql-mysql-fdw (2.6.1-2) unstable; urgency=medium

  * Upload for PostgreSQL 14.
  * B-D on postgresql-server-dev-all only.

 -- Christoph Berg <myon@debian.org>  Thu, 21 Oct 2021 09:49:46 +0200

postgresql-mysql-fdw (2.6.1-1) unstable; urgency=medium

  * New upstream version 2.6.1.
  * Accept regression output variation for maximum fetch_size on 32bit.
  * Fix watch file.

 -- Christoph Berg <myon@debian.org>  Tue, 28 Sep 2021 17:28:40 +0200

postgresql-mysql-fdw (2.5.5-2) unstable; urgency=medium

  * debian/tests: Start mariadb service when available.

 -- Christoph Berg <myon@debian.org>  Thu, 14 Jan 2021 17:25:46 +0100

postgresql-mysql-fdw (2.5.5-1) unstable; urgency=medium

  * New upstream version 2.5.5.

 -- Christoph Berg <myon@debian.org>  Tue, 17 Nov 2020 11:16:37 +0100

postgresql-mysql-fdw (2.5.4-2) unstable; urgency=medium

  * Upload for PostgreSQL 13.
  * Remove -fcommon workaround again (gcc 10 fix).
  * Use dh --with pgxs.
  * R³: no.

 -- Christoph Berg <myon@debian.org>  Mon, 19 Oct 2020 15:21:05 +0200

postgresql-mysql-fdw (2.5.4-1) unstable; urgency=low

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Christoph Berg ]
  * New upstream version.
  * Fix regression tests to work with mariadb as well.
  * Remove some tests with output that varies a lot between MySQL versions.

 -- Christoph Berg <myon@debian.org>  Mon, 03 Aug 2020 13:59:24 +0200

postgresql-mysql-fdw (2.5.3-2) unstable; urgency=medium

  * Fix build failure with gcc 10. (Closes: #957700)
  * DH 13.

 -- Christoph Berg <myon@debian.org>  Wed, 29 Jul 2020 15:58:48 +0200

postgresql-mysql-fdw (2.5.3-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Tue, 01 Oct 2019 09:39:12 +0200

postgresql-mysql-fdw (2.5.2-1) unstable; urgency=medium

  * New upstream version.
  * Add debian/gitlab-ci.yml.

 -- Christoph Berg <myon@debian.org>  Fri, 27 Sep 2019 12:36:04 +0200

postgresql-mysql-fdw (2.5.1-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 29 Nov 2018 10:30:16 +0100

postgresql-mysql-fdw (2.5.0-1) unstable; urgency=medium

  * New upstream version.
  * Upload for PostgreSQL 11.
  * Move maintainer address to team+postgresql@tracker.debian.org.

 -- Christoph Berg <myon@debian.org>  Fri, 19 Oct 2018 22:39:10 +0200

postgresql-mysql-fdw (2.4.0-1) unstable; urgency=medium

  * New upstream version.
  * Move packaging repository to salsa.debian.org

 -- Christoph Berg <myon@debian.org>  Sat, 17 Mar 2018 16:46:53 +0100

postgresql-mysql-fdw (2.3.0-2) unstable; urgency=medium

  * Ignore "Test failure with PG10: ERROR: unrecognized node type: 217" for
    now: https://github.com/EnterpriseDB/mysql_fdw/issues/147

 -- Christoph Berg <christoph.berg@credativ.de>  Fri, 01 Dec 2017 11:15:51 +0100

postgresql-mysql-fdw (2.3.0-1) unstable; urgency=medium

  * New upstream version with PostgreSQL 10 support.
  * Drop postgresql-mysql-fdw-doc package, it contained only README.md which
    is also included in the main package anyway.
  * Add new required table to MySQL test schema.

 -- Christoph Berg <myon@debian.org>  Sun, 10 Sep 2017 18:42:43 +0200

postgresql-mysql-fdw (2.2.0-2) unstable; urgency=medium

  * Use default-mysql-server in test dependencies, stretch doesn't have
    mysql-server.

 -- Christoph Berg <myon@debian.org>  Sun, 19 Mar 2017 20:57:02 +0100

postgresql-mysql-fdw (2.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.
  * Change INFO on established mysql connection to DEBUG1; the user should not
    be bothered with it in normal operation, and moreover, INFO is not
    possible to suppress using client_min_messages for having reproducible
    regression test results (the message contains the mysql server version).

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 12 Jan 2017 10:02:48 +0100

postgresql-mysql-fdw (2.1.2-4) unstable; urgency=medium

  * Switch to default-libmysqlclient-dev. (Closes: #845897, #848293)

 -- Christoph Berg <myon@debian.org>  Sat, 17 Dec 2016 18:15:18 +0100

postgresql-mysql-fdw (2.1.2-3) unstable; urgency=medium

  * Team upload with 9.6 support.

 -- Christoph Berg <christoph.berg@credativ.de>  Tue, 27 Sep 2016 12:37:38 +0200

postgresql-mysql-fdw (2.1.2-2) unstable; urgency=medium

  * Add libmysqlclient-dev to Depends. libmysqlclient.so is loaded at runtime
    using dlopen(). Spotted by Lev Dragunov, thanks!

 -- Christoph Berg <myon@debian.org>  Tue, 17 May 2016 23:45:59 +0200

postgresql-mysql-fdw (2.1.2-1) unstable; urgency=medium

  * New upstream version with 9.5 support.
  * debian/tests: Add new table and disable unshare.
  * Ignore test results on squeeze, precise, and wily.

 -- Christoph Berg <myon@debian.org>  Sat, 23 Jan 2016 11:28:15 +0100

postgresql-mysql-fdw (2.0.1-1) unstable; urgency=low

  * Initial release (Closes: #785453).

 -- Markus Wanner <markus.wanner@datahouse.ch>  Sun, 13 Sep 2015 21:12:59 +0200
